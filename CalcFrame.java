import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class CalcFrame extends JFrame {
    JTextArea display = new JTextArea();
    JTextArea displayAnswer = new JTextArea();
    JLabel label = new JLabel("Your expression:");
    JLabel label2 = new JLabel("Answer:");
    JPanel buttonPanel = new JPanel(new GridLayout(5, 3));
    JPanel inputOutputPanel = new JPanel(new GridLayout(4, 1));
    JButton button0 = new JButton("0");
    JButton button1 = new JButton("1");
    JButton button2 = new JButton("2");
    JButton button3 = new JButton("3");
    JButton button4 = new JButton("4");
    JButton button5 = new JButton("5");
    JButton button6 = new JButton("6");
    JButton button7 = new JButton("7");
    JButton button8 = new JButton("8");
    JButton button9 = new JButton("9");
    JButton buttonSum = new JButton("+");
    JButton buttonLeft = new JButton("(");
    JButton buttonRight = new JButton(")");
    JButton buttonBack = new JButton("C");
    JButton buttonDivide = new JButton("/");
    JButton buttonSub = new JButton("-");
    JButton buttonMul = new JButton("*");
    JButton buttonStart = new JButton("=");
    boolean isAll = false;
    int firstValue = 0;
    String operation = "+";

    CalcFrame() {
        setBounds(400, 400, 400, 400);
        button0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "0");
            }
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "1");
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "2");
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "3");
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "4");
            }
        });
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "5");
            }
        });
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "6");
            }
        });
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "7");
            }
        });
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "8");
            }
        });
        button9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "9");
            }
        });
        buttonRight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + ")");
            }
        });
        buttonLeft.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "(");
            }
        });
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try
                {

                    String temp = display.getText();
                    display.setText(temp.substring(0, temp.length() - 1));
                }catch (IndexOutOfBoundsException ex){

                }
            }
        });
        buttonSum.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "+");
            }
        });
        buttonMul.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "*");
            }
        });
        buttonDivide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "/");
            }
        });
        buttonSub.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (isAll) {
                    display.setText("");
                    isAll = false;
                }
                display.setText(display.getText() + "-");
            }
        });
        buttonStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    displayAnswer.setText(new Integer(PPN.eval(display.getText())).toString());
                    isAll = true;
                } catch (ArithmeticException ex) {
                    JOptionPane.showMessageDialog(null, "Divided by 0", "Exception: " + ex.getMessage(), JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Check your expression", "Exception: " + ex.getMessage(), JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        setLayout(new BorderLayout());
        add(inputOutputPanel, BorderLayout.PAGE_START);
        add(buttonPanel, BorderLayout.CENTER);
        add(buttonStart, BorderLayout.SOUTH);

        inputOutputPanel.add(label);
        inputOutputPanel.add(display);
        inputOutputPanel.add(label2);
        inputOutputPanel.add(displayAnswer);
        buttonPanel.add(button0);
        buttonPanel.add(button1);
        buttonPanel.add(button2);
        buttonPanel.add(button3);
        buttonPanel.add(button4);
        buttonPanel.add(button5);
        buttonPanel.add(button6);
        buttonPanel.add(button7);
        buttonPanel.add(button8);
        buttonPanel.add(button9);
        buttonPanel.add(buttonSum);
        buttonPanel.add(buttonSub);
        buttonPanel.add(buttonMul);
        buttonPanel.add(buttonBack);
        buttonPanel.add(buttonDivide);
        buttonPanel.add(buttonLeft);
        buttonPanel.add(buttonRight);

        setVisible(true);
    }

    public static void main(String[] args) {
        new CalcFrame();
    }
}

